﻿using FluentAssertions;
using NUnit.Framework;

using System;
using System.Collections.Generic;

namespace Lesson1
{
    [TestFixture]
    public class ListTests
    {
        [Test]
        public void AddSuccessfull()
        {
            //arrange
            var list = new List<int> {1, 2};

            //act
            list.Add(3);

            //assert
            list.Count.Should().Be(3);
            list[2].Should().Be(3);
        }

        [Test]
        public void InsertSuccessfull()
        {
            //arrange
            var list = new List<int> { 1, 3 };

            //act
            list.Insert(1, 2);

            //assert
            list.Count.Should().Be(3);
            list[1].Should().Be(2);
        }

        [Test]
        public void InsertError_IndexGreaterThanCount()
        {
            //arrange
            var list = new List<int> { 1, 3 };

            //act
            Action action = () => list.Insert(5, 2);

            //assert
            action.Should().Throw<ArgumentOutOfRangeException>();
        }

        [Test]
        public void InsertError_IndexLessThan0()
        {
            //arrange
            var list = new List<int> { 1, 3 };

            //act
            Action action = () => list.Insert(-1, 2);

            //assert
            action.Should().Throw<ArgumentOutOfRangeException>();
        }
    }
}